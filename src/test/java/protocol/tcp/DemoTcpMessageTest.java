package protocol.tcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import org.apache.commons.codec.binary.Hex;
import org.jetlinks.core.message.codec.EncodedMessage;
import org.jetlinks.core.message.property.ReadPropertyMessage;
import tcp.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tcp.message.AuthRequest;
import tcp.message.FireAlarm;
import tcp.message.ReadProperty;
import java.time.LocalDateTime;

class DemoTcpMessageTest {

    @Test
    void test() {
        DemoTcpMessage message = DemoTcpMessage.of(MessageType.AUTH_REQ, AuthRequest.of(111111111, null));
        byte[] data = message.toBytes();
        System.out.println(Hex.encodeHexString(data));
        DemoTcpMessage decode = DemoTcpMessage.of(data);
        System.out.println(decode);
        Assertions.assertEquals(message.getType(), decode.getType());
        Assertions.assertArrayEquals(message.getData().toBytes(), decode.getData().toBytes());

    }

    @Test
    void encodeTest() {
        DemoTcpMessageCodec demoTcpMessageCodec = new DemoTcpMessageCodec(null);
        ReadPropertyMessage readPropertyMessage = new ReadPropertyMessage();
        readPropertyMessage.setCode("10001");
        readPropertyMessage.setDeviceId("111111");
        readPropertyMessage.setMessageId("test");
        readPropertyMessage.setTimestamp(LocalDateTime.now().getNano());
        DemoTcpMessage of = DemoTcpMessage.of(MessageType.READ_PROPERTY, ReadProperty.of(readPropertyMessage));
        EncodedMessage simple = EncodedMessage.simple(of.toByteBuf());
        ByteBuf byteBuf = simple.getPayload();
        byte[] payload = ByteBufUtil.getBytes(byteBuf, 0, byteBuf.readableBytes(), false);
        DemoTcpMessage message = DemoTcpMessage.of(payload);
        System.out.println(message.getType().getText());
        ReadProperty data = (ReadProperty) message.getData();
        System.out.println(data.getReadPropertyMessage().toString());
    }



    @Test
    void encodeEvent() {
        int [] bytes = new int[6];
        bytes[0] = (byte) 99 ;
        bytes[1] = (byte) 12 ;
        bytes[2] = (byte) 31 ;
        bytes[3] = (byte) 23 ;
        bytes[4] = (byte) 59 ;
        bytes[5] = (byte) 59 ;
        DemoTcpMessage demoTcpMessage = DemoTcpMessage.of(MessageType.FIRE_ALARM,
                FireAlarm.builder()
                        .rechargeableEnergyStorageSubsystems((byte)1)
                        .commandUnit((byte)1)
                        .dataCollectionTime(bytes)
                        .ICCID("1234567489")
                        .loginSerialNumber((byte)4)
                        .dataUnitLength((short) 32)
                        .rechargeableEnergyStorageSystemCode("ABB980317")
                        .rechargeableEnergyStorageSystemCodeLength((byte)"ABB980317".length())
                        .uniquelyIdentifies("111111")
                        .build());
        byte[] data = demoTcpMessage.toBytes();
        System.out.println(demoTcpMessage);
        System.out.println(Hex.encodeHexString(data));
        DemoTcpMessage decode = DemoTcpMessage.of(data);
        System.out.println(decode);
        Assertions.assertEquals(demoTcpMessage.getType(), decode.getType());
        Assertions.assertArrayEquals(demoTcpMessage.getData().toBytes(), decode.getData().toBytes());
    }
}