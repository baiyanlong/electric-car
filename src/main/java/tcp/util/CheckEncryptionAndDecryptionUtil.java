package tcp.util;




import org.jetlinks.core.utils.BytesUtils;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 类说明:BBS 异或校验 和 加解密
 *
 * @author byl
 * @since 2020/9/27
 */
public class CheckEncryptionAndDecryptionUtil {


    private static final char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static String[] binaryArray =
            {"0000","0001","0010","0011",
                    "0100","0101","0110","0111",
                    "1000","1001","1010","1011",
                    "1100","1101","1110","1111"};


    /**
     * 将16进制字符串转换为byte[]
     *
     * @param str
     * @return
     */
    public static byte[] toBytes(String str) {
        if(str == null || str.trim().equals("")) {
            return new byte[0];
        }
        str = str.length() % 2 == 0 ?  str : str+"0";
        byte[] bytes = new byte[str.length() / 2];
        for(int i = 0; i < str.length() / 2; i++) {
            String subStr = str.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }

        return bytes;
    }
    public static String bytesToHexFun(byte[] bytes) {
        // 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
        for(byte b : bytes) { // 使用除与取余进行转换
            if(b < 0) {
                a = 256 + b;
            } else {
                a = b;
            }

            buf[index++] = HEX_CHAR[a / 16];
            buf[index++] = HEX_CHAR[a % 16];
        }
        int k = 0;
        for (int i = buf.length -1 ; i > 0 ; i--) {
            if(buf[i] == '0'){
                k++;
            }else{
                break;
            }
        }
        char[] chars = Arrays.copyOf(buf, buf.length - k);
        return new String(chars);
    }
    /**
     * 亦或校验
     *
     * @param datas type
     * @return 异或校验之后的值
     */
    public static byte [] getXor(byte[] datas , int type) {
        byte temp =  0;
        int start = 2;
        int end = datas.length;
        byte[] xorAfter = new byte[datas.length];
        xorAfter[0] = datas[0];
        xorAfter[1] = datas[1];
        if(type == 1){
            for (int i = start , k = i + 1; i < end - 1; i++ , k++) {
                temp =  datas[ k ];
                temp ^= datas[i];
                datas[i] = temp;
                xorAfter[i] = temp;
            }
        }else if (type == 2)  {
            for (int i = end - start, k = i + 1 ; i > start; i--, k --) {
                temp =  datas[ k ];
                temp ^= datas[ i ];
                datas[i] = temp;
                xorAfter[i] = temp;
            }
        }
        xorAfter[end - 1] = datas[end - 1];
        return xorAfter;
    }

    public static byte[] binaryToHex(byte [] binary){
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < binary.length; i++) {
            buf.append(binary[i]);
        }
        StringBuffer hexBuf = new StringBuffer();
        for (int i = 0 , k = 0; i < buf.length(); i += 4,k++) {
            hexBuf.append(Long.toHexString(Long.parseLong(buf.substring(i, i + 4), 2)));
        }
        return hexStringToBinary(hexBuf.toString());
    }

    /**
     *
     * @param bArray 十进制byte
     * @return 转换为二进制字符串
     */
    public static String bytes2BinaryStr(byte[] bArray){

        String outStr = "";
        int pos = 0;
        for(byte b:bArray){
            //高四位
            pos = (b&0xF0)>>4;
            outStr+=binaryArray[pos];
            //低四位
            pos=b&0x0F;
            outStr+=binaryArray[pos];
        }
        return outStr;

    }




        /**
         *
         * @param hexString
         * @return 将十六进制转换为十进制字节数组
         */
    public static byte[] hexStringToBinary(String hexString){
        if (hexString.length() < 1) {
            return null;
        } else {
            byte[] result = new byte[hexString.length() / 2];
            int j = 0;
            for(int i = 0; i < hexString.length(); i+=2) {
                int res = Integer.parseInt(hexString.substring(i, i + 2), 16);
                result[j++] = (byte)res;
            }
            return result;
        }
    }

    /**
     *
     * @param bytes 十六进制byte []
     * @return 二进制的byte[]
     */
    public static byte[] hexBytesToBinaryBytes(byte[] bytes) {
        String s = bytes2BinaryStr(bytes);
        char[] chars = s.toCharArray();
        byte [] arr =  new byte [chars.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (byte) (chars[i] - '0');
        }
        return arr;
    }

    public static void main(String[] args) {
        byte [] a = new byte[10];
        a[0] = (byte)0xA;
        a[1] = (byte)0x1;
        a[2] = (byte)0x6;
        a[3] = (byte)0x2;
        a[4] = (byte)0x9;
        a[5] = (byte)0xF;
        a[6] = (byte)0xC;
        a[7] = (byte)0x7;
        a[8] = (byte)0x3F;
        a[9] = (byte)0xFB;
//        byte[] binaryBytes = CheckEncryptionAndDecryptionUtil.hexBytesToBinaryBytes(a);
//        byte[] xor = CheckEncryptionAndDecryptionUtil.getXor(binaryBytes, 1);
//        System.out.println(Arrays.toString(binaryBytes));
////        System.out.println(Arrays.toString(xor));
////        System.out.println(Arrays.toString(binaryToHex(xor)));
//        byte[] bytes = binaryToHex(binaryBytes);
//        System.out.println(Arrays.toString(bytes));
//        byte aByte = bytes[bytes.length - 1];
//        System.out.println(aByte);
//        int i = Byte.toUnsignedInt(aByte);
//        short abb = 250;
//        System.out.println(abb);
//        int aShort = Integer.parseInt(Integer.toHexString(i)+"FB",16);
//        System.out.println(aShort);
//        int i = BytesUtils.beToInt(a, 8, 2);
//        System.out.println(i);
//
//        System.arraycopy(a, 2+1, a, 0,
//                a.length-3);
//        System.out.println(Arrays.toString(a));
//        String s = Integer.toBinaryString(BytesUtils.beToInt(a, 8, 1));
//        String front = s.substring(0,s.length() - 4);
//        s= s.substring(s.length()-5,s.length()-1);
//        System.out.println(front + "      "+s);

        long aa = 980317;
//        System.out.println(Arrays.toString(toBytes(aa)));
//        byte[] bytes1 = toBytes(aa);
//        System.out.println(bytesToHexFun(bytes1));
//        byte[] bytes = {-85,-71,-128,49};
//        System.out.println(Arrays.toString(bytes));
//        System.out.println(bytesToHexFun(bytes));
        byte[] bytes = BytesUtils.longToLe(aa);
        System.out.println(Arrays.toString(bytes));
        long l = BytesUtils.leToLong(bytes, 0, bytes.length);
        System.out.println(l);
    }
}
