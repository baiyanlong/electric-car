package tcp.message;

import lombok.*;
import org.apache.commons.codec.binary.Hex;
import org.hswebframework.web.id.IDGenerator;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.event.EventMessage;
import org.jetlinks.core.utils.BytesUtils;
import tcp.TcpDeviceMessage;
import tcp.TcpPayload;
import tcp.util.CheckEncryptionAndDecryptionUtil;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
/**
 * 车辆登入
 */
public class FireAlarm implements TcpPayload, TcpDeviceMessage {


    //数据采集时间
    private int[] dataCollectionTime;

    //登入流水号
    private int loginSerialNumber;

    //ICCID
    private String ICCID;

    //可充电储能子系统数
    private int  rechargeableEnergyStorageSubsystems;

    //可充电储能系统编码长度
    private int rechargeableEnergyStorageSystemCodeLength;

    //可充电储能系统编码
    private  String rechargeableEnergyStorageSystemCode;

    //唯一表示符
    private String uniquelyIdentifies;

    //命令单元
    private byte commandUnit;


    //数据单元长度
    private int dataUnitLength;



    @Override
    public DeviceMessage toDeviceMessage() {
        EventMessage message = new EventMessage();
        message.setEvent("vehicle_login");
        message.setMessageId(IDGenerator.SNOW_FLAKE_STRING.generate());
        Map<String, Object> map = new HashMap<>();
        map.put("dataCollectionTime", dataCollectionTime);
        map.put("loginSerialNumber", loginSerialNumber);
        map.put("ICCID", ICCID);
        map.put("rechargeableEnergyStorageSubsystems", rechargeableEnergyStorageSubsystems);
        map.put("rechargeableEnergyStorageSystemCodeLength", rechargeableEnergyStorageSystemCodeLength);
        map.put("rechargeableEnergyStorageSystemCode", rechargeableEnergyStorageSystemCode);
        message.setData(map);
        message.setDeviceId(uniquelyIdentifies);
        return message;

    }

    @SneakyThrows
    @Override
    public byte[] toBytes() {
        byte[] data = new byte[2 + 19 + 4 + 6 + 2 + 20 + 1 + 1 + rechargeableEnergyStorageSystemCode.length()];
        BytesUtils.numberToLe(data, commandUnit, 2, 1);
        byte [] chars = BytesUtils.longToLe(Long.valueOf(uniquelyIdentifies));
        System.arraycopy(chars,0,data,4,chars.length);
        BytesUtils.numberToLe(data, dataUnitLength, 22, 1);
        for (int i = 0; i < dataCollectionTime.length; i++) {
            BytesUtils.numberToLe(data, dataCollectionTime[i], 24+i, 1);
        }
        BytesUtils.numberToLe(data, loginSerialNumber, 30, 2);
        byte [] ICCIDDataBytes = BytesUtils.longToLe(Long.valueOf(ICCID,16));
        System.arraycopy(ICCIDDataBytes,0,data,32,ICCIDDataBytes.length);
        BytesUtils.numberToLe(data, rechargeableEnergyStorageSubsystems, 52, 1);
        BytesUtils.numberToLe(data, rechargeableEnergyStorageSystemCodeLength, 53, 1);
        byte [] rechargeableEnergyStorageSystemCodeDataBytes = CheckEncryptionAndDecryptionUtil.toBytes(rechargeableEnergyStorageSystemCode);
        System.arraycopy(rechargeableEnergyStorageSystemCodeDataBytes,0,data,54,rechargeableEnergyStorageSystemCodeDataBytes.length);
        return data;
    }

    @Override
    public void fromBytes(byte[] bytes, int offset) {
        System.out.println(Hex.encodeHexString(bytes));
        int command = BytesUtils.leToInt(bytes, offset+2, 1);
        String deviceid = BytesUtils.leToLong(bytes,offset+4,17)+"";
        setUniquelyIdentifies(deviceid);
        setDataUnitLength(BytesUtils.leToInt(bytes, offset + 22, 1));
        int[] timeBytes = new int[6];
        for (int i = 0; i < timeBytes.length; i++) {
            timeBytes[i] = BytesUtils.beToInt(bytes,offset + 24 + i , 1);
        }
        setDataCollectionTime(timeBytes);
        int loginSerialNumber =BytesUtils.leToInt(bytes,offset+30,2);
        setLoginSerialNumber(loginSerialNumber);
        if(command == 1){
            //车辆登入
            setCommandUnit((byte)0x01);
            setICCID(Long.toHexString(BytesUtils.leToLong(bytes,offset+32,20)));
            setRechargeableEnergyStorageSubsystems(BytesUtils.beToInt(bytes, offset+ 52, 1));
            setRechargeableEnergyStorageSystemCodeLength(BytesUtils.beToInt(bytes, offset+ 53, 1));
            byte [] bytesLast = new byte[bytes.length - (offset + 54)];
            System.arraycopy(bytes,offset+ 54,bytesLast,0,bytes.length - (offset + 54));
            setRechargeableEnergyStorageSystemCode(CheckEncryptionAndDecryptionUtil.bytesToHexFun(bytesLast));
        }
    }

}
